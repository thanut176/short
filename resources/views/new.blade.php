@extends('layout.app')
@section('content')
    <br>

    <form method="post" action="{{ url('/') }}">
        @csrf
        <br>
        <div style="text-align: center">
        <h1 >Create new ShortURL</h1>
        </div>
        <br>
        <div class="container p-5" >

            <div class="row justify-content-md-center">
                {{--            <input type="text" name="title" class="form-control" value="{{ old('title') }}">--}}
                <div class="col-10">
                    <input type="text" name="long_url" class="form-control" placeholder="PASTE LONG URL">
                </div>
                <br>
                <br>

                <button type="submit" class="btn btn-secondary col-6">CREATE SHORT URL</button>

            </div>

        </div>
    </form>
    <br>
{{--    <form method="get" action="{{ url('/home') }}">--}}
{{--        <button type="submit" class="btn btn-danger" style="margin-left: 20px;">BACK</button>--}}
{{--    </form>--}}

@endsection
